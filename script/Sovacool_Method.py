import json
import os
import sys
from _collections_abc import Iterable
from datetime import datetime
from functools import reduce
from typing import List, Tuple, Union

import matplotlib
import numpy
import pandas
import requests
import statsmodels.api as sm
import statsmodels.formula.api as smf
import statsmodels.stats.api as sms
from matplotlib import pyplot as plt
from pandas import DataFrame, Series
from scipy import stats
from statsmodels.compat import lzip

import Plots
from utils import Utils
from utils.file import FileUtils
from utils.matplotlib import FigureUtils
from utils.pandas import PandasIndexes, PandasSelection

# Just to display maximum data in the terminal
# numpy.set_printoptions(threshold=sys.maxsize)
# pandas.set_option("display.max_rows", None, "display.max_columns", None)

# General parameters
dpi = 360
size = (10, 6)

matplotlib.use("agg")

# Method
def SovacoolMethod(
    start: datetime, end: datetime, title: str, filename: str, dataFilename: str,
):

    print("Periode : " + str(start) + " - " + str(end))

    # Data
    ## Get global energy consumption
    df = Utils.getData(dataFilename)
    ## Order data
    df = df[["CO2", "GDP", "nuclear", "renewable"]]

    # Control
    ## Plots histogram values
    print("Plots histogram values")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    fig.suptitle("Distribution, {title}".format_map({"title": title,}),)

    subplot_position = [221, 222, 223, 224]
    for index, columnName in enumerate(df):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        Plots.plotHistogram(
            filename="./results/{filename}_Histogram.png".format_map({"filename": filename}),
            values=df[columnName],
            title=columnName,
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    ## Plot Correlation
    print("Plot Correlation")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(9, 5), axSubplot=None)
    plt.suptitle("Cross correlation, {title}".format_map({"title": title,}),)

    def plotCorrelation(
        df: DataFrame, xName: str, yName: str, xlabel: str, ylabel: str, title: str, axSubplot: int
    ):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=axSubplot)

        x = df[xName]
        y = df[yName]

        Plots.plotCrossCorrelation(
            filename="./results/{filename}_Relation.png".format_map({"filename": filename}),
            x=x,
            y=y,
            xlabel=xlabel,
            ylabel=ylabel,
            title=title,
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plotCorrelation(
        df=df,
        xName="GDP",
        yName="CO2",
        title="GDP vs CO2",
        xlabel="GDP [$/cap]",
        ylabel="CO2 [t/cap]",
        axSubplot=131,
    )
    plotCorrelation(
        df=df,
        xName="nuclear",
        yName="CO2",
        title="nuclear vs CO2",
        xlabel="nuclear [%]",
        ylabel=None,
        axSubplot=132,
    )
    plotCorrelation(
        df=df,
        xName="renewable",
        yName="CO2",
        title="renewable vs CO2",
        xlabel="renewable [%]",
        ylabel=None,
        axSubplot=133,
    )

    plt.close("all")

    ## Plot relation between ren and nuc
    print("Plot nuclear over renewable")
    plt.close("all")  # To be sure everything is close

    ## Plot relation between ren and nuc
    Plots.plotScatter(
        filename="./results/{filename}_ScatterNuclearRenewable.png".format_map(
            {"filename": filename}
        ),
        x=df["nuclear"],
        y=df["renewable"],
        xlim=(0, 100),
        ylim=(0, 100),
        xlabel="Nuclear production share",
        ylabel="Renewable production share",
        polygon=[[0, 0], [0, 100], [100, 0]],
        title="Nuclear production compared to renewable production for {title}".format_map(
            {"title": title,}
        ),
    )

    plt.close("all")

    ## Compute Z-score
    df_untransformed = df

    df = (df - df.mean()) / df.std(ddof=0)
    df = df.rename(
        columns={
            "renewable": "renewable_zscore",
            "nuclear": "nuclear_zscore",
            "GDP": "GDP_zscore",
            "CO2": "CO2_zscore",
        }
    )
    df = df[["CO2_zscore", "GDP_zscore", "nuclear_zscore", "renewable_zscore"]]

    ## Z-Score
    print("Plot Z-score")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Distributions, {title}".format_map({"title": title,}),)

    subplot_position = [221, 222, 223, 224]
    listParameter = [
        ("CO2", "CO2_zscore"),
        ("GDP", "GDP_zscore"),
        ("nuclear", "nuclear_zscore"),
        ("renewable", "renewable_zscore"),
    ]
    xlabels = [("t/cap", "z-score"), ("$/cap", "z-score"), ("%", "z-score"), ("%", "z-score")]

    for index, columnName in enumerate(df):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        d_v = df_untransformed[listParameter[index][0]]
        d_z = df[listParameter[index][1]]

        Plots.plotHistogramTwoAxes(
            filename="./results/{filename}_HistogramTwoAxes.png".format_map({"filename": filename}),
            values=(d_v, d_z),
            title=d_v.name,
            xlabel=xlabels[index],
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    ## Vérification de non auto correlation
    def correlationMatrice(data: DataFrame):

        ### Rho
        def calculate_rho(df):
            # https://stackoverflow.com/a/45507587
            df = df.dropna()._get_numeric_data()
            dfcols = pandas.DataFrame(columns=df.columns)
            rhos = dfcols.transpose().join(dfcols, how="outer")
            for r in df.columns:
                for c in df.columns:
                    rhos[r][c] = stats.pearsonr(df[r], df[c])[0]
            return rhos

        ###  P-Value
        def calculate_pvalues(df):
            # https://stackoverflow.com/a/45507587
            df = df.dropna()._get_numeric_data()
            dfcols = pandas.DataFrame(columns=df.columns)
            pvalues = dfcols.transpose().join(dfcols, how="outer")
            for r in df.columns:
                for c in df.columns:
                    pvalues[r][c] = stats.pearsonr(df[r], df[c])[1]
            return pvalues

        matrix_rho = calculate_rho(data)
        pVal = calculate_pvalues(data)

        return matrix_rho, pVal

    matrix_rho, pVal = correlationMatrice(df)
    print("Rho")
    print(matrix_rho)
    print("P-Value")
    print(pVal)

    # Regression multiple
    ## Step 1
    formula = "CO2_zscore ~ GDP_zscore"
    print("Step 1 : " + formula)
    model_1 = smf.ols(formula=formula, data=df,)
    res_1 = model_1.fit()
    title_1 = formula
    # print(res_1.summary())
    # print(res_1.resid)

    ## Step 2
    formula = "CO2_zscore ~ GDP_zscore + nuclear_zscore"
    print("Step 2 : " + formula)
    model_2 = smf.ols(formula=formula, data=df,)
    res_2 = model_2.fit()
    title_2 = formula
    # print(res_2.summary())
    # print(res_2.resid)

    ## Step 3
    formula = "CO2_zscore ~ GDP_zscore + nuclear_zscore + renewable_zscore"
    print("Step 3 : " + formula)
    model_3 = smf.ols(formula=formula, data=df,)
    res_3 = model_3.fit()
    title_3 = formula
    # print(res_3.summary())
    # print(res_3.resid)

    ## Step 4
    formula = "CO2_zscore ~ GDP_zscore + nuclear_zscore + renewable_zscore + GDP_zscore * (nuclear_zscore + renewable_zscore)"
    print("Step 4 : " + formula)
    model_4 = smf.ols(formula=formula, data=df,)
    res_4 = model_4.fit()
    title_4 = formula
    # print(res_4.summary())
    # print(res_4.resid)

    # Tests
    modelList = [
        (model_1, res_1, title_1),
        (model_2, res_2, title_2),
        (model_3, res_3, title_3),
        (model_4, res_4, title_4),
    ]

    ## Influence
    print("## Influence")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Influence plot, {title}".format_map({"title": title,}),)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):
        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        Plots.plotInfluence(
            filename="./results/{filename}_Influence.png".format_map({"filename": filename}),
            result=res,
            title=subtitle,
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    ## Residus distribution
    print("## Residus Distribution")
    plt.close("all")  # To be sure everything is close

    for index, (model, res, subtitle) in enumerate(modelList):

        fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
        plt.suptitle(
            "Residus correlation, {title}\n{subtitle}".format_map(
                {"title": title, "subtitle": subtitle}
            ),
        )

        y = res.resid
        y.name = "residus"

        subplot_position = [221, 222, 223, 224]
        for index_name, name in enumerate(df):

            ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index_name])

            Plots.plotCrossCorrelation(
                filename="./results/{filename}_ResidusCrossCorrelation_model{model}.png".format_map(
                    {"filename": filename, "model": str(index + 1)}
                ),
                x=df[name],
                y=y,
                xlabel=name,
                ylabel="Residual",
                title=name.replace("_zscore", ""),
                fig=fig,
                ax=ax,
                artists=artists,
            )

        plt.close("all")

    ## Residus vs fitted
    print("## Residus vs fitted")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Residus correlation with fitted values, {title}".format_map({"title": title}),)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        Plots.plotCrossCorrelation(
            filename="./results/{filename}_ResidusCrossCorrelation_ResidusFitted.png".format_map(
                {"filename": filename}
            ),
            x=res.fittedvalues,
            y=y,
            xlabel="Fitted",
            ylabel="Residual",
            title="model "
            + str(index + 1),  # subtitle, # subtitle too large or cut it on multiple lines
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    ## Mean values
    print("## Mean values")
    print("Mean : " + str(res.resid.mean()))
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Mean errors per countries on residus, {title}".format_map({"title": title}),)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        countryMean = res.resid.groupby("country").mean()
        countryMean = countryMean.sort_values()

        Plots.plot(
            filename="./results/{filename}_MeanError_Country.png".format_map(
                {"filename": filename}
            ),
            values=countryMean.sort_values(),
            title="model "
            + str(index + 1),  # subtitle, # subtitle too large or cut it on multiple lines
            labels=list(countryMean.index),
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    ## Test de normalité : Kolmogorov-Smirnov
    print("## Test de normalité : Kolmogorov-Smirnov")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Residus distribution, {title}".format_map({"title": title}),)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        Plots.plot(
            filename="./results/{filename}_ResidusDistribution.png".format_map(
                {"filename": filename}
            ),
            values=res.resid,
            title="model "
            + str(index + 1),  # subtitle, # subtitle too large or cut it on multiple lines
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    # Henry
    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])
        plt.suptitle("Normal probability plot, {title}".format_map({"title": title}),)

        KolmogorovSmirnov_res = stats.kstest(res.resid, "norm")
        Plots.plotNormalProbability(
            filename="./results/{filename}_NormalProbability.png".format_map(
                {"filename": filename}
            ),
            data=res.resid,
            title="{subtitle},KS:{KS}, p-value:{pvalue}".format_map(
                {
                    "subtitle": "model "
                    + str(index + 1),  # subtitle, # subtitle too large or cut it on multiple lines
                    "KS": "{:.4f}".format(KolmogorovSmirnov_res.statistic),
                    "pvalue": "{:.4f}".format(KolmogorovSmirnov_res.pvalue)
                    if KolmogorovSmirnov_res.pvalue > 0.001
                    else "<0.001",
                }
            ),
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    # Normal distribution
    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Residus distribution, {title}".format_map({"title": title}),)

    subplot_position = [(421, 423), (422, 424), (425, 427), (426, 428)]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax1 = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index][0])
        ax2 = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index][1])

        KolmogorovSmirnov_res = stats.kstest(res.resid, "norm")
        Plots.plotNormalDistribution(
            filename="./results/{filename}_NormalDistribution.png".format_map(
                {"filename": filename}
            ),
            data=res.resid,
            # title="{subtitle}\nKolmogorov-Smirnov: stat:{stat}, p-value:{pvalue}".format_map(
            #     {
            #         "subtitle": "model " + str(index+1),#subtitle, # subtitle too large or cut it on multiple lines
            #         "stat": "{:.4f}".format(KolmogorovSmirnov_res.statistic),
            #         "pvalue": "{:.4f}".format(KolmogorovSmirnov_res.pvalue),
            #     }
            # ),
            title="model " + str(index + 1),
            fig=fig,
            ax1=ax1,
            ax2=ax2,
            artists=artists,
        )

    plt.close("all")

    ## homoscedasticity
    print("## homoscedasticity")
    plt.close("all")  # To be sure everything is close

    fig, ax, artists = FigureUtils.getFigure(size=(10, 6), axSubplot=None)
    plt.suptitle("Homoscedasticity, {title}".format_map({"title": title}),)

    subplot_position = [221, 222, 223, 224]
    for index, (model, res, subtitle) in enumerate(modelList):

        ax = FigureUtils.addSubplot(fig=fig, axSubplot=subplot_position[index])

        Plots.plotCrossCorrelation(
            filename="./results/{filename}_Homoscedasticity.png".format_map({"filename": filename}),
            x=res.fittedvalues,
            y=numpy.abs(res.resid),
            xlabel="Fitted",
            ylabel="Absolute Residual",
            displaySigma=False,
            title="model " + str(index + 1),
            fig=fig,
            ax=ax,
            artists=artists,
        )

    plt.close("all")

    print("End")


if __name__ == "__main__":

    print("Enr group")
    SovacoolMethod(
        start=pandas.Timestamp(datetime(1990, 1, 1), tz="UTC"),
        end=pandas.Timestamp(datetime(2004, 1, 1), tz="Europe/Brussels"),
        title='"Renewable group" - TF 1',
        filename="Sovacool/Sovacool_Enr_TF1",
        dataFilename="./data/Sovacool/nature_energy_sup_ENR_TF1.xlsx",
    )
    SovacoolMethod(
        start=pandas.Timestamp(datetime(1990, 1, 1), tz="UTC"),
        end=pandas.Timestamp(datetime(2004, 1, 1), tz="Europe/Brussels"),
        title='"Renewable group" - TF 2',
        filename="Sovacool/Sovacool_Enr_TF2",
        dataFilename="./data/Sovacool/nature_energy_sup_ENR_TF2.xlsx",
    )

    print("nuclear group")
    SovacoolMethod(
        start=pandas.Timestamp(datetime(1990, 1, 1), tz="UTC"),
        end=pandas.Timestamp(datetime(2004, 1, 1), tz="Europe/Brussels"),
        title='"nuclear group" - TF 1',
        filename="Sovacool/Sovacool_NUC_TF1",
        dataFilename="./data/Sovacool/nature_energy_sup_NUC_TF1.xlsx",
    )
    SovacoolMethod(
        start=pandas.Timestamp(datetime(1990, 1, 1), tz="UTC"),
        end=pandas.Timestamp(datetime(2004, 1, 1), tz="Europe/Brussels"),
        title='"nuclear group" - TF 2',
        filename="Sovacool/Sovacool_NUC_TF2",
        dataFilename="./data/Sovacool/nature_energy_sup_NUC_TF2.xlsx",
    )
