import json
import os
from functools import reduce

import pandas
import requests
from pandas import DataFrame

from utils import Utils
from utils.file import FileUtils


def export(data: DataFrame, filepath: str):

    # data = data.reset_index()

    parent = FileUtils.getParent(filepath)
    FileUtils.createFolder(folder=parent)
    data = data.to_csv(filepath)


if __name__ == "__main__":

    # Get global energy consumption
    df_elec = Utils.getData("./data/ELECTRICITY_GENERATION_TWH.json")

    df_enr = Utils.getData(
        "./data/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.json", powerPlant="NONE"
    )
    df_hydro = Utils.getData("./data/HYDRO_PRODUCTION_MTOE.json", powerPlant="NONE")
    df_enr_ratio = (df_enr + df_hydro) / df_elec

    df_nuc = Utils.getData("./data/NUCLEAR_PRODUCTION_MTOE.json")
    df_nuc_ratio = df_nuc / df_elec

    # CO2
    df_Pop = Utils.getData("./data/POPULATION.json")
    df_CO2 = Utils.getData("./data/FOSSIL_CO2_EMISSIONS_MT.json")
    df_CO2_ratio = df_CO2 / df_Pop

    # GDP
    df_GDP = Utils.getData("./data/GDP_2010_USD.json")
    df_GDP_Ratio = df_GDP / df_Pop

    df_elec = df_elec.rename(columns={"value": "ELECTRICITY_GENERATION_TWH"})
    export(df_elec, "./data/export/ELECTRICITY_GENERATION_TWH.csv")

    df_enr = df_enr.rename(columns={"value": "GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH"})
    export(df_enr, "./data/export/GEOTH_SOLAR_WIND_TIDE_ELECTRICITY_GENERATION_TWH.csv")

    df_hydro = df_hydro.rename(columns={"value": "HYDRO_PRODUCTION_TWH"})
    export(df_hydro, "./data/export/HYDRO_PRODUCTION_TWH.csv")

    df_enr_ratio = df_enr_ratio.rename(columns={"value": "ELECTRICITY_GENERATION_TWH"})
    export(
        df_enr_ratio, "./data/export/GEOTH_SOLAR_WIND_TIDE_HYDRO_ELECTRICITY_GENERATION_RATIO.csv"
    )

    df_nuc = df_nuc.rename(columns={"value": "NUCLEAR_PRODUCTION_TWH"})
    export(df_nuc, "./data/export/NUCLEAR_PRODUCTION_TWH.csv")

    df_nuc_ratio = df_nuc_ratio.rename(columns={"value": "NUCLEAR_PRODUCTION_RATIO"})
    export(df_nuc_ratio, "./data/export/NUCLEAR_PRODUCTION_RATIO.csv")

    df_Pop = df_Pop.rename(columns={"value": "POPULATION_MILLION"})
    export(df_Pop, "./data/export/POPULATION_MILLION.csv")

    df_CO2 = df_CO2.rename(columns={"value": "FOSSIL_CO2_EMISSIONS_MT"})
    export(df_CO2, "./data/export/FOSSIL_CO2_EMISSIONS_MT.csv")

    df_CO2_ratio = df_CO2_ratio.rename(columns={"value": "FOSSIL_CO2_EMISSIONS_TonnePerCapita"})
    export(df_CO2_ratio, "./data/export/FOSSIL_CO2_EMISSIONS_TonnePerCapita.csv")

    df_CO2 = df_CO2.rename(columns={"value": "ELECTRICITY_GENERATION_TWH"})
    export(df_CO2, "./data/export/GDP_2010_USD.csv")
    df_GDP_Ratio = df_GDP_Ratio.rename(columns={"value": "GDP_2010_kUSDPerCapital"})
    export(df_GDP_Ratio, "./data/export/GDP_2010_kUSDPerCapital.csv")

    dfs = [
        df_elec,
        df_enr,
        df_hydro,
        df_enr_ratio,
        df_nuc,
        df_nuc_ratio,
        df_Pop,
        df_CO2,
        df_CO2_ratio,
        df_GDP,
        df_GDP_Ratio,
    ]
    df_whole = pandas.concat(dfs)

    df_whole = reduce(
        lambda left, right: pandas.merge(left, right, on=["country", "date"], how="outer"), dfs
    )
    export(df_whole, "./data/export/WholeData.csv")
