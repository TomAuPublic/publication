from pandas.core.series import Series


def applyFunction(data, function):

    data = Series(data.reset_index().apply(function, axis=1).values, index=data.index)

    return data
