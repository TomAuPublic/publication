import pandas as pd


def getIndexeUniqueValues(data, level):

    return data.index.unique(level=level)


def getIndexeValues(data, level):

    return data.index.get_level_values(level)


def addIndexeLevel(data, levelName, levelValue):

    # Convert index to dataframe
    old_idx = data.index.to_frame()

    # Insert new level at specified location
    old_idx.insert(0, levelName, levelValue)

    # Convert back to MultiIndex
    data.index = pd.MultiIndex.from_frame(old_idx)

    return data
