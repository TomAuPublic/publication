from _collections_abc import Iterable
from datetime import datetime
from typing import Callable, Dict, List, Union

import numpy
import pandas
from pandas import DataFrame
from pandas.core.groupby import GroupBy
from pandas.core.series import Series

from utils.pandas import PandasIndexes


def select(
    data: DataFrame,
    parameter: Union[List[str], str],
    value: Union[List[str], str],
    operator: str = "or",
) -> DataFrame:
    """
    Select data following the parameter and the value provided
    
    data:
        data to filter
    country:
        country to select
    """

    if isinstance(data, Series):
        data = data.to_frame()

    queryString = ""
    if (
        isinstance(parameter, Iterable)
        and not isinstance(parameter, str)
        and isinstance(value, Iterable)
        and not isinstance(value, str)
    ):
        zipValues = list(zip(parameter, value))
        queryString = (" " + operator + " ").join([p + ' == "' + v + '"' for p, v in zipValues])
    elif isinstance(parameter, str) and isinstance(value, Iterable) and not isinstance(value, str):
        queryString = (" " + operator + " ").join(
            [str(parameter) + ' == "' + str(v) + '"' for v in value]
        )
    else:
        queryString = str(parameter) + ' == "' + str(value) + '"'

    data = data.query(queryString)
    return data


def selectBetween(
    data: DataFrame,
    parameter: str,
    start: datetime,
    end: datetime,
    includeStart: bool = True,
    includeEnd: bool = True,
):
    """
    Select data following the parameter and the value provided
    
    data:
        data to filter
    country:
        country to select
    """

    wasSerie = False
    previousUnit = None
    if isinstance(data, Series):
        wasSerie = True
        # Get the unit
        previousUnit = Units.getUnit(data)
        data = data.to_frame()

    queryString = ""
    if includeStart:
        queryString += str(parameter) + ' >= "' + start + '"'
    else:
        queryString += str(parameter) + ' > "' + start + '"'

    queryString += " and "

    if includeEnd:
        queryString += str(parameter) + ' <= "' + end + '"'
    else:
        queryString += str(parameter) + ' < "' + end + '"'

    data = data.query(queryString)

    if wasSerie:
        # Return a Series
        data = data.iloc[:, 0]
        # Add the unit
        data = Units.setUnit(data, previousUnit)

    return data


def selectCountry(data: DataFrame, country: List[str]) -> DataFrame:
    """
    Select only the country
    
    data:
        data to filter
    country:
        country to select
    """

    data = select(data, "country", country)
    return data


def __naiveDatetime(date: Union[str, datetime]) -> DataFrame:

    date = pandas.to_datetime(date)

    if date.tzinfo is None:
        date.tz_localize("Europe/Brussels")

    return date


def selectDatetime(data: DataFrame, date: List[datetime]) -> DataFrame:
    """
    Select only the datetime
    
    data:
        data to filter
    date:
        date to select
    """

    datetime = __naiveDatetime(date)

    data = select(data, "date", str(datetime))
    return data


def selectBetweenDatetime(
    data: DataFrame,
    datetimeStart: datetime,
    datetimeEnd: datetime,
    includeStart: bool = True,
    includeEnd: bool = True,
) -> DataFrame:
    """
    Select only the datetime
    
    data:
        data to filter
    category:
        datetime to select
    """

    datetimeStart = __naiveDatetime(datetimeStart)
    datetimeEnd = __naiveDatetime(datetimeEnd)

    data = selectBetween(
        data, "date", str(datetimeStart), str(datetimeEnd), includeStart, includeEnd
    )
    return data


def dropEmptyIndex(data: DataFrame, level: List[str], verbose: bool = False, **kwargs):

    dataGroup = data.groupby(level=level)

    toReturn = []
    for key, group in dataGroup:

        # Method too slow
        #         if not group.any():
        #
        #             if verbose:
        #                 print("Drop : " + str(key))
        #
        #             data.drop(key, inplace=True)
        # #             data = data[~data.index.isin(key)]

        if group.any():

            if verbose:
                print("Keep : " + str(key))

            toReturn.append(group)

    toReturn = pandas.concat(toReturn)
    toReturn.name = data.name

    return toReturn


def getCommonCountries(*datas: List[DataFrame]):

    countries = None
    for idx in range(len(datas)):
        if countries is None:
            countries = PandasIndexes.getIndexeUniqueValues(datas[0], index.country)
        else:
            countries = countries.intersection(
                other=PandasIndexes.getIndexeUniqueValues(datas[idx], index.country), sort=False
            )

    return countries


def selectCountries(*datas: List[DataFrame]):

    countries = getCommonCountries(*datas)
    return tuple([selectCountry(data, countries) for data in datas])


def convertCategory(
    data: DataFrame, categories: Dict[str, str], groupingOperation: Callable = numpy.sum
) -> DataFrame:
    """Convert catgories to another one based on the dict provided

    Parameters
    ----------
    data: DataFrame
        DataFrame to convert
    categories: Dict[str, str]
        Categories convertion to use

    Return
    ------
    DataFrame"""

    columns = list(data.index.names)

    data = data.reset_index()
    data[index.category + "_tmp"] = data[index.category].map(categories)
    data = data.set_index([*columns, index.category + "_tmp"])

    columns.remove(index.category)
    data = data.groupby([*columns, index.category + "_tmp"]).agg(groupingOperation)
    data.index.names = [*columns, index.category]

    return data
