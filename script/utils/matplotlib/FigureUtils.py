from typing import Any, List, Tuple, Optional

import matplotlib
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, subplot

matplotlib.use("agg")


def getFigure(
    size: Tuple[int, int], axSubplot: Optional[int] = 111
) -> Tuple[figure, subplot, List[Any]]:

    fig = plt.figure(figsize=size)
    #     fig.set_size_inches(size)

    artists: List[Any] = []

    plt.style.context("fast")

    # Define a subplot to be used
    if axSubplot:
        ax = addSubplot(fig=fig, axSubplot=axSubplot)
    else:
        ax = None

    return fig, ax, artists


def addSubplot(fig, axSubplot: int):

    ax = fig.add_subplot(axSubplot)
    ax.xaxis.grid(True)
    ax.yaxis.grid(True)

    return ax


def addLegend(ax):

    lgd = ax.legend(loc="upper left", bbox_to_anchor=(1, 1), ncol=1, markerscale=2)
    return lgd
